# Build Instructions:

* Clone the source repository from Bitbucket
    * On the command line, enter:
        * git clone https://przevalski@bitbucket.org/przewalski/deallinktaskfrontend.git

* Change **url** in **js\app.js** to your url from backend (APP_DOMAIN)
    * https://bitbucket.org/przewalski/deallinktaskbackend

* On the command line, enter:
    * **npm install**
    * **npm run dev**
    
* Launch the application on localhost

* The video shows how everything works
    * https://www.youtube.com/watch?v=tdm6enlhrb0&feature=youtu.be
